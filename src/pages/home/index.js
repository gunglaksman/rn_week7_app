import React, {Component, useCallback} from 'react';
import {
  View,
  Text,
  StatusBar,
  ScrollView,
  ImageBackground,
  Image,
  Button,
  TouchableOpacity,
  RefreshControl,
  Alert,
} from 'react-native';
import {warna} from '../../color';
import ui from '../../style';
import axios from 'axios';
import InternetConnectionAlert from 'react-native-internet-connection-alert';

export default class index extends Component {
  constructor() {
    super();
    this.state = {
      latestUpload: [],
      refreshing: false,
      contohState: 1,
      timePassed: false,
      detil: [],
      genres: {},
      genresDetil: [
        {
          id: 28,
          name: 'Action',
        },
        {
          id: 12,
          name: 'Adventure',
        },
        {
          id: 16,
          name: 'Animation',
        },
        {
          id: 35,
          name: 'Comedy',
        },
        {
          id: 80,
          name: 'Crime',
        },
        {
          id: 99,
          name: 'Documentary',
        },
        {
          id: 18,
          name: 'Drama',
        },
        {
          id: 10751,
          name: 'Family',
        },
        {
          id: 14,
          name: 'Fantasy',
        },
        {
          id: 36,
          name: 'History',
        },
        {
          id: 27,
          name: 'Horror',
        },
        {
          id: 10402,
          name: 'Music',
        },
        {
          id: 9648,
          name: 'Mystery',
        },
        {
          id: 10749,
          name: 'Romance',
        },
        {
          id: 878,
          name: 'Science Fiction',
        },
        {
          id: 10770,
          name: 'TV Movie',
        },
        {
          id: 53,
          name: 'Thriller',
        },
        {
          id: 10752,
          name: 'War',
        },
        {
          id: 37,
          name: 'Western',
        },
      ],
    };
  }

  componentDidMount = async () => {
    StatusBar.setBackgroundColor(warna.primary);
    const hasil = await axios.get('http://code.aldipee.com/api/v1/movies/');
    const ops = [];

    hasil.data.results.map(item => {
      ops.push(axios.get(`http://code.aldipee.com/api/v1/movies/${item.id}`));
    });

    // let alamat = 'http://code.aldipee.com/api/v1/movies/602223';
    // let alamat2 = 'http://code.aldipee.com/api/v1/movies/602223';
    // const janji = axios.get(alamat);
    // const janji2 = axios.get(alamat);
    Promise.all(ops).then(values => {
      this.setState({
        detil: values,
      });
      // console.log(this.state.detil);
    });
    // console.log(ops);
    await this.delay(3000);
    this.setState({
      latestUpload: hasil.data.results,
      timePassed: true,
    });
    const arr = [];
    this.state.detil.map(item => {
      arr.push(item.data.genres);
      this.setState({
        genres: arr,
      });
      // console.log(item.data.genres);
    });
    // console.log(this.state.latestUpload);
  };

  delay = ms => new Promise(res => setTimeout(res, ms));
  shouldComponentUpdate(nextProps, nextState) {
    return nextState.latestUpload !== this.state.latestUpload;
  }

  onRefresh = async () => {
    this.setState({refreshing: true});
    const hasil = await axios.get('http://code.aldipee.com/api/v1/movies/');
    this.setState({
      latestUpload: hasil.data.results,
      refreshing: false,
    });
  };

  filterArray = id => {
    switch (id) {
      case 28:
    }
  };

  votingStar(vote) {
    if (vote >= 5 && vote < 7) {
      return (
        <>
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
        </>
      );
    } else if (vote >= 7 && vote < 9) {
      return (
        <>
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
        </>
      );
    } else if (vote >= 9 && vote <= 10) {
      return (
        <>
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/fillStar.png')}
          />
        </>
      );
    } else {
      return (
        <>
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
          <Image
            style={ui.star}
            source={require('../../../assets/emptyStar.png')}
          />
        </>
      );
    }
  }

  render() {
    return this.state.timePassed === false ? (
      <ImageBackground
        source={require('../../../assets/splash.png')}
        style={ui.background}
      />
    ) : (
      <View style={ui.container}>
        <InternetConnectionAlert
          onChange={connectionState => {
            console.log('Connection State: ', connectionState);
          }}>
          <ScrollView
            refreshControl={
              <RefreshControl
                refreshing={this.state.refreshing}
                onRefresh={this.onRefresh}
              />
            }>
            <View style={ui.recommendedContainer}>
              <Text style={ui.judul}>Recommended</Text>
              <ScrollView horizontal={true}>
                {this.state.latestUpload
                  .sort((a, b) => (a.vote_average > b.vote_average ? 1 : -1))
                  .reverse()
                  .map((item, index) => (
                    <View key={index} style={ui.recommendedComponent}>
                      <View style={ui.recommendedCard}>
                        <ImageBackground
                          source={{uri: `${item.poster_path}`}}
                          resizeMode="contain"
                          imageStyle={{borderRadius: 10}}
                          style={ui.imagebg}
                        />
                      </View>
                      <View style={ui.recommendedText}>
                        <Text style={ui.subJudul}>{item.title}</Text>
                        <Text style={ui.subJudulGenre}>
                          {item.genre_ids.slice(0, 2).map(items => {
                            return (
                              this.state.genresDetil.find(x => x.id === items)
                                .name + ', '
                            );
                            // console.log(a);
                          })}
                        </Text>
                      </View>
                    </View>
                  ))}
              </ScrollView>
            </View>
            <View style={ui.listContainer}>
              <Text style={ui.judul}>Latest Upload</Text>
              <ScrollView>
                <View style={ui.listComponent}>
                  {this.state.latestUpload
                    .sort((a, b) =>
                      new Date(a.release_date).getTime() >
                      new Date(b.release_date).getTime()
                        ? 1
                        : -1,
                    )
                    .reverse()
                    .map((item, index) => (
                      <View key={index} style={ui.listCard}>
                        <View style={ui.overlay}>
                          <Image
                            style={ui.imageCard}
                            source={{
                              uri: `${item.poster_path}`,
                            }}
                          />
                          <View style={ui.containerDeskripsi}>
                            <Text style={ui.judulFilm}>{item.title}</Text>
                            <Text style={ui.tanggal}>{item.release_date}</Text>
                            <View style={{flex: 1, flexDirection: 'column'}}>
                              <View style={{flex: 0.6, flexDirection: 'row'}}>
                                {this.votingStar(item.vote_average)}
                                <Text style={ui.starFont}>
                                  {item.vote_average}
                                </Text>
                              </View>
                              <View style={{flex: 1, flexDirection: 'row'}}>
                                <Text style={ui.fontGenre}>
                                  {item.genre_ids.map(items => {
                                    return (
                                      this.state.genresDetil.find(
                                        x => x.id === items,
                                      ).name + ', '
                                    );
                                    // console.log(a);
                                  })}
                                </Text>
                              </View>
                            </View>
                            <TouchableOpacity
                              onPress={() => {
                                this.props.navigation.navigate('DetailMovie', {
                                  idFilm: item.id,
                                });
                              }}
                              style={ui.buttonMore}>
                              <Text style={ui.fontButton}>Show More</Text>
                            </TouchableOpacity>
                          </View>
                        </View>
                      </View>
                    ))}
                </View>
              </ScrollView>
            </View>
          </ScrollView>
        </InternetConnectionAlert>
      </View>
    );
  }
}
